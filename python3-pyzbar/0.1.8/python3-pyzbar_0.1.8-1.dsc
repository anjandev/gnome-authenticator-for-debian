Format: 3.0 (quilt)
Source: python3-pyzbar
Binary: python3-python3-pyzbar, python-python3-pyzbar-doc
Architecture: all
Version: 0.1.8-1
Maintainer: debian <anjan@momi.ca>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), dh-python, python3-setuptools, python3-all, python3-numpy
Package-List:
 python-python3-pyzbar-doc deb doc optional arch=all
 python3-python3-pyzbar deb unknown optional arch=all
Checksums-Sha1:
 af1640e5ef143d48b1bd5064d683c85090f3b452 41562 python3-pyzbar_0.1.8.orig.tar.gz
 4be0a9e1a4500c79653036654044a88622c5b313 8616 python3-pyzbar_0.1.8-1.debian.tar.xz
Checksums-Sha256:
 f51c82c2864f8e5a8d44f55853e027f8cbc592324b7afffa62100f2f9c54cbdb 41562 python3-pyzbar_0.1.8.orig.tar.gz
 7d0f87e16ad79f465d39d230c96e022e5b4566650fe30ef4ee2811a592c90e1b 8616 python3-pyzbar_0.1.8-1.debian.tar.xz
Files:
 5ce6c5713be0bddd0d5e4727f767c35f 41562 python3-pyzbar_0.1.8.orig.tar.gz
 392e608705b68daa73474892fa683fc5 8616 python3-pyzbar_0.1.8-1.debian.tar.xz
