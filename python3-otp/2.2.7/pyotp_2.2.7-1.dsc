Format: 3.0 (quilt)
Source: pyotp
Binary: python3-pyotp, python-pyotp-doc
Architecture: all
Version: 2.2.7-1
Maintainer: Anjandev Momi <anjan@momi.ca>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), dh-python, python3-setuptools, python3-all
Package-List:
 python-pyotp-doc deb doc optional arch=all
 python3-pyotp deb unknown optional arch=all
Checksums-Sha1:
 1cbc82bed8a21eba4305b9edd799e1bcad361feb 11416 pyotp_2.2.7.orig.tar.gz
 6871980da5eb11455457b412e736e19cf23d5b50 8000 pyotp_2.2.7-1.debian.tar.xz
Checksums-Sha256:
 be0ffeabddaa5ee53e7204e7740da842d070cf69168247a3d0c08541b84de602 11416 pyotp_2.2.7.orig.tar.gz
 12476ff1c4a2e26303770585ce28125b8da314d9c41fdb7f838063d9e6b08a22 8000 pyotp_2.2.7-1.debian.tar.xz
Files:
 7d3f89d8c4805e0facf6ebb6f9cebd29 11416 pyotp_2.2.7.orig.tar.gz
 2de454e56b6b1698ac4774c59da9217f 8000 pyotp_2.2.7-1.debian.tar.xz
